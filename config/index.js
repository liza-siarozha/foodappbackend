require('dotenv').config();

const CONFIG = {};

CONFIG.app = process.env.APP || 'dev';
CONFIG.port = process.env.PORT || '27017';

CONFIG.db_host = process.env.DB_HOST || 'localhost';
CONFIG.db_port = process.env.DB_PORT || '27017';
CONFIG.db_name = process.env.DB_NAME || 'FoodApp';

CONFIG.mail_service = process.env.MAIL_SERVICE || 'YourMailService';
CONFIG.mail_user = process.env.MAIL_USER || 'YourSMTPLogin';
CONFIG.mail_password = process.env.MAIL_PASSWORD || 'YourSMTPPassword';

// MailGun
CONFIG.mailgun_api_key = process.env.MAILGUN_API_KEY || '';
CONFIG.mailgun_domain = process.env.MAILGUN_DOMAIN || '';

CONFIG.jwt_encryption = process.env.JWT_ENCRYPTION || 'YourSecretKey';

module.exports = CONFIG;
