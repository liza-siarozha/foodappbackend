/* eslint-disable consistent-return */
/* eslint-disable no-undef */
/* eslint-disable no-return-assign */
/* eslint-disable no-use-before-define */
import $ from 'jquery';
import 'fullcalendar';
import 'materialize-css';
import axios from 'axios';
import './css/materialize.min.css';
import './css/fullcalendar.css';
import './css/style.css';

const axiosInstance = axios.create({
  baseURL: 'http://localhost:3000/',
});

axiosInstance.interceptors.request.use((config) => {
  const accessToken = localStorage.getItem('token');
  if (accessToken !== null) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  return config;
}, err => Promise.reject(err));

axiosInstance.interceptors.response.use(res => res,
  async (err) => {
    const refreshToken = localStorage.getItem('refreshToken');
    if (!refreshToken || err.response.status !== 403 || err.config.retry) {
      return Promise.reject(err);
    }
    const { data } = await axiosInstance.post('/auth/refresh', { refreshToken });
    localStorage.setItem('token', data.accessToken);
    localStorage.setItem('refreshToken', data.refreshToken);

    const newRequest = {
      ...err.config,
      retry: true,
    };
    return axiosInstance(newRequest);
  });

const bodyContainer = document.getElementById('container');
const navbar = document.getElementById('navbar');

const errorsRenderDOM = (errors) => {
  const errorsDOM = document.getElementById('errors');
  let userId;
  let errHTML = '';
  errors.forEach((err) => {
    if (typeof err === 'object') {
      errHTML += `
      <p>${err.message}</p>
      <p>
        <a href="localhost:3000/auth/verify/gen/${err.id}" id="link-generation">Generate new link</a>
      </p>`;
      userId = err.id;
    } else {
      errHTML += `<p>${err}</p>`;
    }
  });
  errorsDOM.innerHTML = errHTML;
  const linkGenerator = document.getElementById('link-generation');
  if (linkGenerator) {
    linkGenerator.addEventListener('click', handleGenerateLink(userId));
  }
};

const messageRenderDOM = (message) => {
  const messageContainer = document.createElement('div');
  messageContainer.className = 'message-container';
  messageContainer.innerText = message;
  return messageContainer.outerHTML;
};

const renderLoggedIn = (userRole = 'admin') => {
  navbar.innerHTML = '';
  const ranking = document.createElement('a');
  ranking.setAttribute('href', '/ranking');
  ranking.setAttribute('id', 'ranking');
  ranking.innerText = 'Ranking';
  ranking.addEventListener('click', renderRanking);
  const categories = document.createElement('a');
  categories.setAttribute('href', '/categories');
  categories.setAttribute('id', 'categories');
  categories.innerText = 'Categories';
  categories.addEventListener('click', renderCategories);
  const orders = document.createElement('a');
  orders.setAttribute('href', '/orders');
  orders.setAttribute('id', 'orders');
  orders.innerText = 'Orders';
  orders.addEventListener('click', renderCalendar);
  const admin = document.createElement('a');
  admin.setAttribute('href', '/admin');
  admin.setAttribute('id', 'admin');
  admin.innerText = 'Admin Dashboard';
  admin.addEventListener('click', renderAdmin);
  const logout = document.createElement('a');
  logout.setAttribute('href', '/logout');
  logout.setAttribute('id', 'logout');
  logout.innerText = 'Log Out';
  logout.addEventListener('click', handleLogout);
  const contentLinks = document.createElement('div');
  contentLinks.className = 'content-links';
  if (userRole === 'admin') {
    contentLinks.appendChild(ranking);
    contentLinks.appendChild(categories);
    contentLinks.appendChild(orders);
    contentLinks.appendChild(admin);
    navbar.appendChild(contentLinks);
  } else if (userRole === 'client') {
    contentLinks.appendChild(ranking);
    contentLinks.appendChild(categories);
    contentLinks.appendChild(orders);
    navbar.appendChild(contentLinks);
  }
  navbar.appendChild(logout);
};

const renderLoggedOut = () => {
  navbar.innerHTML = '';
  const register = document.createElement('a');
  register.setAttribute('href', '/register');
  register.setAttribute('id', 'register');
  register.innerText = 'Register';
  register.addEventListener('click', renderRegister);
  const login = document.createElement('a');
  login.setAttribute('href', '/login');
  login.setAttribute('id', 'login');
  login.innerText = 'Log In';
  login.addEventListener('click', renderLogin);
  navbar.appendChild(login);
  navbar.appendChild(register);
  const contentLinks = document.createElement('div');
  contentLinks.className = 'content-links';
};

// EVENT HANDLERS
// GET Requests
const renderRegister = (e) => {
  if (e) {
    e.preventDefault();
  }
  axiosInstance.get('/auth/register')
    .then((res) => {
      bodyContainer.innerHTML = res.data;
      const registerForm = document.getElementById('register-form');
      registerForm.addEventListener('submit', handleRegister);
    });
};

const renderLogin = (e) => {
  if (e) {
    e.preventDefault();
  }
  axiosInstance.get('/auth/login')
    .then((res) => {
      bodyContainer.innerHTML = res.data;
      const loginForm = document.getElementById('login-form');
      loginForm.addEventListener('submit', handleLogin);
    });
};

const renderRanking = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const res = await axiosInstance.get('/ranking');
  bodyContainer.innerHTML = res.data;
  const foods = [...document.getElementsByClassName('food-item')];
  foods.forEach((food) => {
    food.addEventListener('click', renderFoodPage());
  });
};

const renderCategories = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const res = await axiosInstance.get('/categories');
  bodyContainer.innerHTML = res.data;
  initCategoryListeners();
};

const getEvents = async () => {
  const res = await axiosInstance.get('/orders');
  const orders = res.data;
  const events = [];
  orders.forEach((order) => {
    let title = '';
    if (order.foods.length > 0) {
      const foods = order.foods.map(food => food.itemTitle);
      title = foods.join('\n');
    } else {
      title = 'Your order is empty!';
    }
    events.push({
      start: order.date,
      title,
      allDay: true,
      stick: true,
    });
  });
  return events;
};

const initCategoryListeners = () => {
  const foods = [...document.getElementsByClassName('get-food-item')];
  const categoryLikes = [...document.getElementsByClassName('category-like')];
  const foodLikes = [...document.getElementsByClassName('food-like')];
  foods.forEach((food) => {
    food.addEventListener('click', renderFoodPage());
  });
  categoryLikes.forEach((category) => {
    category.addEventListener('click', categoryLike);
  });
  foodLikes.forEach((food) => {
    food.addEventListener('click', foodLikeOutside);
  });
};

const renderFoodPage = foodId => async (e) => {
  if (e) {
    e.preventDefault();
  }
  const id = foodId || e.target.parentElement.id;
  const res = await axiosInstance.get(`/categories/food/${id}`);
  bodyContainer.innerHTML = res.data;
  const foodLike = document.getElementById('food-like');
  const commentForm = document.getElementById('comment-form');
  const editComments = [...document.getElementsByClassName('edit-comment')];
  const deleteComments = [...document.getElementsByClassName('delete-comment')];
  foodLike.addEventListener('click', foodLikeInside(id));
  commentForm.addEventListener('submit', handleAddComment);
  editComments.forEach(el => el.addEventListener('click', handleEditComment(id)));
  deleteComments.forEach(el => el.addEventListener('click', handleDeleteComment(id)));
};

const renderCalendar = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const res = await axiosInstance.get('/orders/calendar');
  bodyContainer.innerHTML = res.data;

  $('#menu-date-container').fullCalendar({
    locale: 'en-gb',
    events: await getEvents(),
    validRange: (nowDate) => {
      const date = new Date();
      const dateLine = new Date();
      dateLine.setHours(10, 0, 0, 0);
      if (date > dateLine) {
        date.setDate(date.getDate() + 1);
        date.setHours(0, 0, 0, 0);
      }
      const start = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
        .toISOString().slice(0, 10);
      return {
        start,
        end: nowDate.clone().add(1, 'months'),
      };
    },
    selectable: true,
    dayClick: async (date) => {
      const dateFormatted = date.format();
      const response = await axiosInstance.get(`/orders/${dateFormatted}`);
      if (response.headers.order === 'false') {
        return renderMenu(dateFormatted, response.data);
      }
      if (response.headers.order === 'true') {
        return renderOrder(dateFormatted, response.data);
      }
    },
  });
};

const renderMenu = (orderDate, resPage) => {
  const container = document.getElementById('menu-date-container');
  container.innerHTML = resPage;
  const menuForm = document.getElementById('menu-form');
  if (menuForm) {
    menuForm.addEventListener('submit', handleAddOrder(orderDate));
  }
};

const renderOrder = (orderDate, resPage) => {
  const container = document.getElementById('menu-date-container');
  container.innerHTML = resPage;
  const editOrder = document.getElementById('edit-order');
  const deleteOrder = document.getElementById('delete-order');
  if (editOrder) {
    editOrder.addEventListener('click', renderOrderEdit(orderDate));
  }
  if (deleteOrder) {
    deleteOrder.addEventListener('click', handleDeleteOrder(orderDate));
  }
};

const renderOrderEdit = orderDate => async (e) => {
  e.preventDefault();
  const container = document.getElementById('menu-date-container');
  const res = await axiosInstance.get(`/orders/${orderDate}`, { headers: { Update: true } });
  container.innerHTML = res.data;
  const updateOrderForm = document.getElementById('edit-menu-form');
  updateOrderForm.addEventListener('submit', handleUpdateOrder(orderDate));
};

const renderAdmin = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const res = await axiosInstance.get('/admin');
  bodyContainer.innerHTML = res.data;
  const categoryForm = document.getElementById('category_form');
  const categoryListadmin = document.getElementById('category_list_admin');
  if (categoryListadmin) {
    const categoriesArr = [...categoryListadmin.children].map(li => li.children[0]);
    categoriesArr.forEach((link) => {
      link.addEventListener('click', renderCategoryPage);
    });
  }
  categoryForm.addEventListener('submit', handleAddCategory);
};

const renderCategoryPage = async (e, id) => {
  if (e) {
    e.preventDefault();
  }
  const categoryId = id || e.target.id;
  const res = await axiosInstance.get(`/admin/${categoryId}`);
  bodyContainer.innerHTML = res.data;

  const updateCategory = document.getElementById(`${categoryId}`);
  const removeCategory = document.getElementById('remove-category');
  const addFoodItem = document.getElementById('add-food');
  const foodItemsList = document.getElementById('category-items');

  if (foodItemsList) {
    const foodItemsArr = [...foodItemsList.children].map(li => li.children[1]);
    foodItemsArr.forEach((foodLink) => {
      foodLink.addEventListener('click', renderAdminFoodPage(null, categoryId));
    });
  }
  updateCategory.addEventListener('click', handleUpdateCategory);
  removeCategory.addEventListener('click', handleRemoveCategory(categoryId));
  addFoodItem.addEventListener('click', handleAddFood(categoryId));
};

const renderAdminFoodPage = (id, categoryId) => async (e) => {
  if (e) {
    e.preventDefault();
  }
  const foodId = id || e.target.id;
  const res = await axiosInstance.get(`/admin/food/${foodId}`);
  bodyContainer.innerHTML = res.data;

  const updateFood = document.getElementById(`${foodId}`);
  const removeFood = document.getElementById('food-remove');
  const deleteComments = [...document.getElementsByClassName('delete-comment')];

  updateFood.addEventListener('click', handleUpdateFood(foodId, categoryId));
  removeFood.addEventListener('click', handleRemoveFood(foodId, categoryId));
  deleteComments.forEach(el => el.addEventListener('click', handleDeleteComment(foodId, true, categoryId)));
};

// POST Requests
const handleRegister = async (e) => {
  e.preventDefault();
  const reqBody = {
    name: e.target.elements[0].value,
    email: e.target.elements[1].value,
    password: e.target.elements[2].value,
    confirmPassword: e.target.elements[3].value,
  };
  const res = await axiosInstance.post('/auth/register', reqBody);
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  return bodyContainer.innerHTML = messageRenderDOM(res.data.message);
};

const handleAddComment = async (e) => {
  e.preventDefault();
  const { id } = e.target[1];
  const reqBody = {
    text: e.target[0].value.trim(),
  };
  const res = await axiosInstance.post(`/categories/food/${id}`, reqBody);
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  renderFoodPage(id)();
};

const handleLogin = async (e) => {
  e.preventDefault();
  const reqBody = {
    email: e.target.elements[0].value,
    password: e.target.elements[1].value,
  };
  const res = await axiosInstance.post('/auth/login', reqBody);
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  localStorage.setItem('token', res.data.tokenPair.accessToken);
  localStorage.setItem('refreshToken', res.data.tokenPair.refreshToken);
  renderLoggedIn(res.data.userRole);
  return bodyContainer.innerHTML = messageRenderDOM('Successfully Logged In');
};

const handleAddOrder = orderDate => async (e) => {
  if (e) {
    e.preventDefault();
  }
  const checkboxes = [...document.querySelectorAll('input[name="foods"]:checked')];
  const foods = checkboxes.map(el => el.value);
  const res = await axiosInstance.post(`/orders/${orderDate}`, { foods });
  renderOrder(orderDate, res.data);
};

const handleGenerateLink = userId => async (e) => {
  e.preventDefault();
  const res = await axiosInstance.get(`/auth/verify/gen/${userId}`);
  bodyContainer.innerHTML = messageRenderDOM(res.data);
};

const handleLogout = async (e) => {
  e.preventDefault();
  const refreshToken = localStorage.getItem('refreshToken');
  const res = await axiosInstance.post('/auth/logout', { refreshToken });
  localStorage.removeItem('token');
  localStorage.removeItem('refreshToken');
  renderLoggedOut();
  return bodyContainer.innerHTML = messageRenderDOM(res.data);
};

const handleAddCategory = async (e) => {
  e.preventDefault();
  const reqBody = {
    title: e.target.elements[0].value,
    description: e.target.elements[1].value,
  };
  const res = await axiosInstance.post('/admin', reqBody);
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  return renderAdmin();
};

const handleAddFood = categoryId => async (e) => {
  e.preventDefault();
  const data = new FormData();
  data.set('itemTitle', document.getElementById('food-title').value);
  data.set('itemDescription', document.getElementById('food-description').value);
  data.set('itemCost', document.getElementById('food-cost').value);
  data.append('itemImg', document.getElementById('food-img').files[0]);
  const res = await axiosInstance.post(`/admin/${categoryId}/food`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  return renderCategoryPage(null, categoryId);
};

const categoryLike = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const reqBody = {
    id: e.target.parentElement.parentElement.id,
  };
  await axiosInstance.post('/categories/like', reqBody);
  renderCategories();
};

const foodLikeOutside = async (e) => {
  if (e) {
    e.preventDefault();
  }
  const reqBody = {
    id: e.target.parentElement.parentElement.id,
  };
  await axiosInstance.post('/categories/food/like', reqBody);
  renderCategories();
};

const foodLikeInside = id => async (e) => {
  if (e) {
    e.preventDefault();
  }
  await axiosInstance.post('/categories/food/like', { id });
  renderFoodPage(id)();
};

// PATCH Requests
const handleUpdateCategory = async (e) => {
  e.preventDefault();
  const reqBody = {
    title: document.getElementById('category-title').value,
    description: document.getElementById('category-description').value,
  };
  const res = await axiosInstance.patch(`/admin/${e.target.id}`, reqBody);
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  return renderCategoryPage(null, e.target.id);
};

const handleUpdateFood = (foodId, categoryId) => async (e) => {
  e.preventDefault();
  const data = new FormData();
  data.set('itemTitle', document.getElementById('food-title').value);
  data.set('itemDescription', document.getElementById('food-description').value);
  data.set('itemCost', document.getElementById('food-cost').value);
  data.append('itemImg', document.getElementById('food-img').files[0]);
  const res = await axiosInstance.patch(`/admin/food/${foodId}`, data, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  if (res.data.errors) {
    return errorsRenderDOM(res.data.errors);
  }
  return renderAdminFoodPage(foodId, categoryId)();
};

const handleUpdateOrder = orderDate => async (e) => {
  e.preventDefault();
  const checkboxes = [...document.querySelectorAll('input[name="foods"]:checked')];
  const foods = checkboxes.map(el => el.value);
  const res = await axiosInstance.patch(`/orders/${orderDate}`, { foods });
  renderOrder(orderDate, res.data);
};

const handleEditComment = foodId => (e) => {
  if (e) {
    e.preventDefault();
  }
  const { id } = e.target.parentElement.parentElement;
  const commentContainer = e.target.parentElement.previousElementSibling;
  const prevComment = commentContainer.children[1];
  const inputElement = document.createElement('input');
  inputElement.setAttribute('type', 'text');
  inputElement.setAttribute('value', prevComment.innerText);
  commentContainer.replaceChild(inputElement, prevComment);

  const buttonsContainer = e.target.parentElement;
  const saveIcon = document.createElement('i');
  saveIcon.classList.add('material-icons');
  saveIcon.classList.add('save-comment');
  saveIcon.innerText = 'save';
  buttonsContainer.removeChild(buttonsContainer.children[0]);
  buttonsContainer.replaceChild(saveIcon, buttonsContainer.children[0]);
  saveIcon.addEventListener('click', handleUpdateComment(id, inputElement, foodId));
};

const handleDeleteComment = (foodId, isAdmin, categoryId) => async (e) => {
  if (e) {
    e.preventDefault();
  }
  const { id } = e.target.parentElement.parentElement;
  await axiosInstance.delete(`/categories/comment/${id}`);
  if (isAdmin) {
    renderAdminFoodPage(foodId, categoryId)();
  } else {
    renderFoodPage(foodId)();
  }
};

const handleUpdateComment = (id, input, foodId) => async (e) => {
  e.preventDefault();
  const text = input.value;
  const res = await axiosInstance.patch(`/categories/comment/${id}`, { text });
  const newComment = document.createElement('span');
  newComment.innerText = res.data.text;
  const buttonsContainer = input.parentElement.nextElementSibling;
  input.parentElement.replaceChild(newComment, input);

  const editIcon = document.createElement('i');
  editIcon.classList.add('material-icons');
  editIcon.classList.add('edit-comment');
  editIcon.innerText = 'edit';
  editIcon.addEventListener('click', handleEditComment(foodId));
  const deleteIcon = document.createElement('i');
  deleteIcon.classList.add('material-icons');
  deleteIcon.classList.add('delete-comments');
  deleteIcon.innerText = 'delete';
  deleteIcon.addEventListener('click', handleDeleteComment(foodId));
  buttonsContainer.replaceChild(editIcon, buttonsContainer.children[0]);
  buttonsContainer.appendChild(deleteIcon);
};

// DELETE Requests
const handleRemoveCategory = categoryId => async (e) => {
  e.preventDefault();
  await axiosInstance.delete(`/admin/${categoryId}`);
  renderAdmin();
};

const handleRemoveFood = (foodId, categoryId) => async (e) => {
  e.preventDefault();
  await axiosInstance.delete(`/admin/food/${foodId}`);
  renderCategoryPage(null, categoryId);
};

const handleDeleteOrder = orderDate => async (e) => {
  e.preventDefault();
  const res = await axiosInstance.delete(`/orders/${orderDate}`);
  renderMenu(orderDate, res.data);
};

renderLoggedOut();
