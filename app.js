const express = require('express');
const logger = require('morgan');
const createError = require('http-errors');
const path = require('path');
const exphbs = require('express-handlebars');
const hbsUtil = require('handlebars-utils');
const cookieParser = require('cookie-parser');
const jwtMiddleware = require('express-jwt-middleware');
const CONFIG = require('./config/index');

// Connect routers
const authRouter = require('./routes/auth');
const adminRouter = require('./routes/admin');
const categoriesRouter = require('./routes/categories');
const ordersRouter = require('./routes/orders');
const rankingRouter = require('./routes/ranking');

const app = express();

// View engine setup
app.engine('hbs', exphbs({
  helpers: {
    inArray(array, value, options) {
      return hbsUtil.value(hbsUtil.indexOf(array, value.toString()) > -1, this, options);
    },
  },
  extname: 'hbs',
  defaultLayout: 'main',
  layoutsDir: `${__dirname}/views/layouts`,
  partialsDir: [
    `${__dirname}/views/partials`,
  ],
}));
app.set('view engine', 'hbs');

// Global middlewares
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/favicon.ico', (req, res) => res.end());

app.get('/', (req, res) => res.render('index'));

// Routers

app.use('/auth', authRouter);
app.use(jwtMiddleware({ secret: CONFIG.jwt_encryption }));
app.use('/admin', adminRouter);
app.use('/orders', ordersRouter);
app.use('/categories', categoriesRouter);
app.use('/orders', ordersRouter);
app.use('/ranking', rankingRouter);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// Error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
