const { compare } = require('bcryptjs');

const { User } = require('../models/User');
require('../db/mongoose');

const validateRegistration = async (req, res, next) => {
  const {
    email, password, confirmPassword, name,
  } = req.body;
  const errors = [];
  // eslint-disable-next-line no-useless-escape
  const emailRe = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

  if (!name) {
    errors.push('Name is required');
  } else if (name.length < 2) {
    errors.push('Name is too short');
  }

  if (!email) {
    errors.push('Email is required');
  } else if (!emailRe.test(email)) {
    errors.push('Invalid email format');
  } else {
    const user = await User.findOne({ email });
    if (user) {
      errors.push('Email already in use');
    }
  }

  if (password.length < 8) {
    errors.push('Password should consist of minimum 8 characters');
  } else if (!/[A-Z]/.test(password)) {
    errors.push('Password must contain uppercase characters');
    if (!/\d/.test(password)) {
      errors.push('Password must contain numbers');
    }
  } else if (password !== confirmPassword) {
    errors.push('Passwords don\'t match');
  }

  if (errors.length > 0) {
    return res.send({ errors });
  }
  return next();
};

const validateAuthorization = async (req, res, next) => {
  const { email, password } = req.body;
  const errors = [];
  // eslint-disable-next-line no-useless-escape
  const emailRe = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

  if (!email) {
    errors.push('Email is required');
  } else if (!emailRe.test(email)) {
    errors.push('Invalid email format');
  } else {
    const user = await User.findOne({ email });
    if (!user) {
      errors.push('User not found');
    } else if (password.length < 8) {
      errors.push('Password is too short');
    } else if (!await compare(password, user.password)) {
      errors.push('Wrong password');
    } else if (!user.validated) {
      // eslint-disable-next-line no-underscore-dangle
      errors.push({ message: 'Please, verify your account', id: user._id });
    }
  }

  if (errors.length > 0) {
    return res.send({ errors });
  }
  return next();
};

const validateCategory = (req, res, next) => {
  const { title, description } = req.body;
  const errors = [];

  if (!title) {
    errors.push('Please, enter category title');
  } else if (title.length < 2) {
    errors.push('Category title must have at least 2 characters');
  }

  if (!description) {
    errors.push('Please, enter category description');
  } else if (description.length < 2) {
    errors.push('Category description must have at least 2 characters');
  }

  if (errors.length > 0) {
    return res.send({ errors });
  }
  return next();
};

const validateFoodItem = (req, res, next) => {
  const { itemTitle, itemDescription, itemCost } = req.body;
  const errors = [];

  if (!itemTitle) {
    errors.push('Enter food title');
  } else if (itemTitle.length < 2) {
    errors.push('Food title must have at least 2 characters');
  }

  if (!itemDescription) {
    errors.push('Enter food description');
  } else if (itemDescription.length < 2) {
    errors.push('Food description must have at least 2 characters');
  }

  if (!itemCost) {
    errors.push('Define food cost');
  }

  if (errors.length > 0) {
    return res.send({ errors });
  }
  return next();
};

module.exports = {
  validateRegistration,
  validateAuthorization,
  validateCategory,
  validateFoodItem,
};
