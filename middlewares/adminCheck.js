const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  const accessToken = req.headers.authorization.split(' ')[1];
  const userRole = jwt.decode(accessToken).data.role;
  if (userRole === 'admin') {
    next();
  } else {
    res.status(401).send("You're not an admin, dummy");
  }
};
