const nodemailer = require('nodemailer');
const mg = require('nodemailer-mailgun-transport');
const CONFIG = require('../config');

/* In case you use different from Mailgun service
const transport = nodemailer.createTransport({
  service: CONFIG.mail_service,
  auth: {
    user: CONFIG.mail_user,
    password: CONFIG.mail_password,
  },
  tls: {
    rejectUnauthorized: false
  }
});
*/

const auth = {
  auth: {
    api_key: CONFIG.mailgun_api_key,
    domain: CONFIG.mailgun_domain,
  },
};
const transport = nodemailer.createTransport(mg(auth));

const sendEmail = (from, subject, to, html) => new Promise((res, rej) => {
  transport.sendMail({
    from, subject, to, html,
  }, (err, info) => {
    if (err) rej(err);
    res(info);
  });
});

const composeEmail = (url, token) => `
  <h2>Hi from FoodApp!</h2>
  <h1>Thank you for registering!</h1>
  <p>Please, confirm your email via link below:</p>
  <a href="${url}${token}">CONFIRM EMAIL</a>
`;

module.exports = {
  sendEmail,
  composeEmail,
};
