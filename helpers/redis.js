const redisClient = require('redis').createClient();
const { promisify } = require('util');

const getAsync = promisify(redisClient.get).bind(redisClient);

redisClient.on('error', (err) => {
  // eslint-disable-next-line no-console
  console.log(`Error ${err}`);
});

const redis = {};

redis.storeRefreshToken = async (userId, token) => {
  let tokensList = await getAsync(userId) || '';
  tokensList += tokensList ? `,${token}` : token;
  // if (!tokensList) {
  //   return res.status(404).send('Refresh tokens list not found');
  // }
  redisClient.set(userId, tokensList);
};

redis.removeRefreshToken = async (userId, token) => {
  const tokensList = await getAsync(userId) || '';
  const tokensArr = tokensList.indexOf(',') !== 1 ? tokensList.split(',') : [tokensList];

  tokensArr.splice(tokensArr.indexOf(token), 1);
  redisClient.set(userId, tokensArr.join());
};

redis.checkRefreshToken = async (userId, token) => {
  // if (!token) {
  //   return res.status(404).send('Refresh token not found');
  // }
  const tokensList = await getAsync(userId);
  return tokensList.indexOf(token) !== -1;
};

redis.dropRefreshTokens = (userId) => {
  redisClient.del(userId);
};

module.exports = redis;
