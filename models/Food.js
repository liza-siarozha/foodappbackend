const mongoose = require('mongoose');

const { Schema } = mongoose;
// Food Items schema
const FoodSchema = new mongoose.Schema({
  category: {
    type: Schema.Types.ObjectId, ref: 'Category',
  },
  itemTitle: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  itemDescription: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  itemCost: {
    type: Number,
    required: true,
    default: 0,
  },
  itemImg: {
    type: String,
    required: true,
    default: 'no-image.png',
  },
  orders: [{
    type: Schema.Types.ObjectId,
    ref: 'Order',
  }],
  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'Like',
  }],
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  }],
});

const Food = mongoose.model('Food', FoodSchema);

module.exports = { Food };
