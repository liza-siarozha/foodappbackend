const mongoose = require('mongoose');
const validator = require('validator');

// Food Items schema
const LikeSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  entity: {
    type: String,
    required: true,
    validate: {
      validator: role => validator.isIn(role, ['category', 'food']),
      message: 'Like should refer to \'category\' or \'food\'',
    },
  },
  entityId: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
});

const Like = mongoose.model('Like', LikeSchema);

module.exports = { Like };
