const mongoose = require('mongoose');

const { Schema } = mongoose;

// Food Items schema
const CommentSchema = new mongoose.Schema({
  user: {
    type: Schema.Types.ObjectId, ref: 'User',
  },
  foodId: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  text: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = { Comment };
