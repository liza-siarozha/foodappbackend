const mongoose = require('mongoose');

const { Schema } = mongoose;

const CategorySchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  foods: [{
    type: Schema.Types.ObjectId,
    ref: 'Food',
  }],
  likes: [{
    type: Schema.Types.ObjectId,
    ref: 'Like',
  }],
});

const Category = mongoose.model('Category', CategorySchema);

module.exports = { Category };
