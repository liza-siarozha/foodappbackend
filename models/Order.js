const mongoose = require('mongoose');

const { Schema } = mongoose;

const OrderSchema = new mongoose.Schema({
  userId: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  date: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  foods: [{
    type: Schema.Types.ObjectId,
    ref: 'Food',
  }],
});

const Order = mongoose.model('Order', OrderSchema);

module.exports = { Order };
