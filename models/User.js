const mongoose = require('mongoose');
const validator = require('validator');

const { Schema } = mongoose;

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    minlength: 1,
    trim: true,
    unique: true,
    validate: {
      validator: validator.isEmail,
      message: '{VALUE} is not a valid email',
    },
  },
  password: {
    type: String,
    require: true,
    minlength: 8,
  },
  name: {
    type: String,
    required: true,
    minlength: 2,
    trim: true,
  },
  role: {
    type: String,
    required: true,
    validate: {
      validator: role => validator.isIn(role, ['client', 'admin']),
      message: 'User role should be \'client\' or \'admin\'',
    },
  },
  emailToken: {
    token: String,
    expiredAt: Date,
  },
  validated: {
    type: Boolean,
    required: true,
  },
  comments: [{
    type: Schema.Types.ObjectId,
    ref: 'Comment',
  }],
});

const User = mongoose.model('User', UserSchema);

module.exports = { User };
