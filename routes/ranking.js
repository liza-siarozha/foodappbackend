/* eslint-disable no-param-reassign */
const express = require('express');
const jwt = require('jsonwebtoken');
const { Food } = require('../models/Food');
require('../db/mongoose'); // Connect to DB

const router = express.Router();

router.get('/', async (req, res) => {
  const amount = 5;
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const date = new Date();
  date.setDate(date.getDate() + 1);
  const orderDate = new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
    .toISOString().slice(0, 10);

  const foods = await Food.find()
    .lean()
    .populate('orders', 'date userId');

  const itterableFoods = foods.map(food => Object.assign({}, food));
  const itterableFoodsSecond = foods.map(food => Object.assign({}, food));
  const dayOrderedFoods = itterableFoods
    .filter((food) => {
      food.orders = food.orders.filter(order => order.date === orderDate);
      return food.orders.length > 0;
    })
    .sort((foodX, foodY) => foodY.orders.length - foodX.orders.length)
    .slice(0, amount);

  const everOrderedFoods = itterableFoodsSecond
    .filter((food) => {
      food.orders = food.orders.filter(order => order.userId === userId);
      return food.orders.length > 0;
    })
    .sort((foodX, foodY) => foodY.orders.length - foodX.orders.length)
    .slice(0, amount);

  const likedFoods = foods
    .filter(food => food.likes.length > 0)
    .sort((foodX, foodY) => foodY.likes.length - foodX.likes.length)
    .slice(0, amount);

  res.render('ranking', {
    layout: false, likedFoods, everOrderedFoods, dayOrderedFoods,
  });
});

module.exports = router;
