/* eslint-disable no-underscore-dangle */
const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const { promisify } = require('util');
const { ObjectID } = require('mongodb');

const removeFile = promisify(fs.unlink);
const renameFile = promisify(fs.rename);

const { validateCategory, validateFoodItem } = require('../middlewares/inputValidation');
const adminCheck = require('../middlewares/adminCheck');
const { Category } = require('../models/Category');
const { Food } = require('../models/Food');
const { Like } = require('../models/Like');
const { Comment } = require('../models/Comment');
const { Order } = require('../models/Order');
const { User } = require('../models/User');
require('../db/mongoose'); // Connect to DB

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const dest = './public/images';
    cb(null, dest);
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

const router = express.Router();
router.use(adminCheck);

const validateId = (res, id, entityTitle) => {
  if (!ObjectID.isValid(id)) {
    return res.status(404).send(`${entityTitle} ID is not valid`);
  }
  return true;
};

const renameImageFood = async (food, oldName) => {
  const extension = oldName.split('.')[1];
  const newName = `${food._id.toString()}.${extension}`;
  await renameFile(`./public/images/${oldName}`, `./public/images/${newName}`);
  // eslint-disable-next-line no-param-reassign
  food.itemImg = newName;
  await food.save();
};

/* Routes */
router.get('/', async (req, res) => {
  const categories = await Category.find({});
  res.render('admin', {
    layout: false, categories,
  });
});

router.post('/', validateCategory, async (req, res) => {
  const { title, description } = req.body;
  const newCategory = new Category({ title, description, items: [] });
  const category = await newCategory.save();
  res.send(category);
});

router.get('/:id', async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Category');
  const category = await Category.findById(id)
    .populate('foods')
    .exec();
  if (!category) return res.status(404).send('Category not found');
  return res.render('admin/category', { layout: false, category });
});

router.patch('/:id', validateCategory, async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Category');
  const { title, description } = req.body;
  const body = { title, description };
  const category = await Category.findByIdAndUpdate(id, { $set: body }, { new: true });
  if (!category) return res.status(404).send('Food item not found');
  return res.render('admin/category', { layout: false, category });
});

router.delete('/:id', async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Category');
  const category = await Category.findByIdAndRemove(id)
    .populate({
      path: 'foods',
      populate: {
        path: 'comments',
      },
    });
  if (!category) return res.status(404).send('Category not found');
  category.foods.forEach(async (food) => {
    if (food.itemImg !== 'no-image.png') {
      await removeFile(`./public/images/${food.itemImg}`);
    }
    await Like.deleteMany({ _id: { $in: food.likes } });
    await Order.updateMany({ _id: { $in: food.orders } }, { $pull: { foods: food._id } });
    food.comments.forEach(async (comment) => {
      await User.updateMany({ _id: comment.user }, { $pull: { comments: comment._id } });
    });
    await Comment.deleteMany({ _id: { $in: food.comments } });
  });
  await Food.deleteMany({ _id: { $in: category.foods } });
  await Like.deleteMany({ _id: { $in: category.likes } });
  return res.send(category);
});

/* EDIT FOOD ITEMS */
router.post('/:categoryId/food', upload.single('itemImg'), validateFoodItem, async (req, res) => {
  const itemImg = (req.file && req.file.filename) || 'no-image.png';
  const { itemTitle, itemDescription, itemCost } = req.body;
  const { categoryId } = req.params;
  // id validation
  validateId(res, categoryId, 'Category');
  const category = await Category.findById(categoryId);
  if (!category) return res.status(404).send('Category not found');
  const food = new Food({
    category: categoryId, itemTitle, itemDescription, itemCost, itemImg,
  });
  await food.save();
  category.foods.push(food);
  await category.save();
  if (itemImg !== 'no-image.png') {
    await renameImageFood(food, req.file.filename);
  }
  return res.render('admin/category', { layout: false, category });
});

router.get('/food/:id', async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Food Item');
  const food = await Food.findById(id)
    .populate({
      path: 'comments',
      populate: {
        path: 'user',
        select: 'name email',
      },
    });
  if (!food) return res.status(404).send('Food item not found');
  return res.render('admin/food', { layout: false, food });
});

router.patch('/food/:id', upload.single('itemImg'), validateFoodItem, async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Food Item');
  const { itemTitle, itemDescription, itemCost } = req.body;
  const itemImg = (req.file && req.file.filename) || 'no-image.png';
  const food = await Food.findById(id);
  const oldName = food.itemImg;
  food.set({
    itemTitle, itemDescription, itemCost, itemImg: itemImg === 'no-image.png' ? oldName : itemImg,
  });
  const updatedFood = await food.save();
  if (itemImg !== 'no-image.png') {
    if (oldName !== 'no-image.png') {
      await removeFile(`./public/images/${oldName}`);
    }
    await renameImageFood(updatedFood, itemImg);
  }
  return res.send(updatedFood);
});

router.delete('/food/:id', async (req, res) => {
  const { id } = req.params;
  // id validation
  validateId(res, id, 'Food Item');
  const food = await Food.findByIdAndDelete(id)
    .populate('comments');
  if (!food) return res.send('Food not found');
  const category = await Category.findOneAndUpdate(
    { _id: food.category },
    { $pull: { foods: food._id } },
    { new: true },
  );
  if (!category) return res.send(404).send('Category not found');
  food.comments.forEach(async (comment) => {
    await User.updateMany({ _id: comment.user }, { $pull: { comments: comment._id } });
  });
  await Comment.deleteMany({ _id: { $in: food.comments } });
  await Like.deleteMany({ _id: { $in: food.likes } });
  await Order.updateMany({ _id: { $in: food.orders } }, { $pull: { foods: food._id } });
  const { itemImg } = food;
  if (itemImg !== 'no-image.png') {
    await removeFile(`./public/images/${itemImg}`);
  }
  return res.send(category);
});

module.exports = router;
