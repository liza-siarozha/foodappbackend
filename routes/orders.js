/* eslint-disable no-underscore-dangle */
const express = require('express');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const { Order } = require('../models/Order');
const { Category } = require('../models/Category');
const { Food } = require('../models/Food');
require('../db/mongoose');
// Connect to DB
const router = express.Router();

router.get('/calendar', async (req, res) => {
  res.render('client/calendar', { layout: false });
});

router.get('/', async (req, res) => {
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const orders = await Order.find({ userId })
    .populate({
      path: 'foods',
      select: 'itemTitle itemDescription itemCost category.title',
      populate: {
        path: 'category',
        select: 'title -_id',
      },
    });
  res.send(orders);
});

router.get('/:date', async (req, res) => {
  const { date } = req.params;
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const order = await Order.findOne({ date, userId })
    .populate({
      path: 'foods',
      select: 'itemTitle itemDescription itemCost category.title',
      populate: {
        path: 'category',
        select: 'title -_id',
      },
    });
  if (!order) {
    const categories = await Category.find({})
      .populate('foods', 'itemTitle itemDescription itemCost');
    res.set('order', false);
    return res.render('client/dailyMenu', { layout: false, categories, update: false });
  }
  if (req.headers.update) {
    const categories = await Category.find({})
      .populate('foods', 'itemTitle itemDescription itemCost');
    res.set('order', false);
    const foodsOrdered = order.foods.map(food => food._id.toString());
    return res.render('client/dailyMenu', {
      layout: false, categories, foodsOrdered, update: true,
    });
  }

  res.set('order', true);
  return res.render('client/dailyOrder', { layout: false, foods: order.foods });
});

router.post('/:date', async (req, res) => {
  const { foods } = req.body;
  const { date } = req.params;
  if (foods.length < 1) {
    return res.send('Choose something to add');
  }
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const orderExisted = await Order.findOne({ date, userId });
  if (orderExisted) {
    return res.send(`Order for ${date} already exists`);
  }
  const order = new Order({
    userId, date, foods: foods.map(food => new mongoose.Types.ObjectId(food)),
  });
  await order.save();
  await Food.updateMany({ _id: { $in: order.foods } }, { $push: { orders: order._id } });
  const newOrder = await Order.findById(order._id)
    .populate({
      path: 'foods',
      select: 'itemTitle itemDescription itemCost category.title',
      populate: {
        path: 'category',
        select: 'title -_id',
      },
    });
  return res.render('client/dailyOrder', { layout: false, foods: newOrder.foods });
});

router.patch('/:date', async (req, res) => {
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const { foods } = req.body;
  if (foods.length < 1) return res.send('Choose something to add');
  const { date } = req.params;
  const order = await Order.findOne({ date, userId });
  const removedFoods = order.foods
    .slice()
    .map(id => id.toString())
    .filter(id => !foods.includes(id));
  const newFoods = foods
    .slice()
    .filter(id => !order.foods
      .map(food => food.toString())
      .includes(id));
  order.set('foods', foods.map(food => new mongoose.Types.ObjectId(food)));
  await order.save();
  await Food.updateMany({ _id: { $in: newFoods } }, { $push: { orders: order._id } });
  await Food.updateMany({ _id: { $in: removedFoods } }, { $pull: { orders: order._id } });
  const updatedOrder = await Order.findById(order._id)
    .populate({
      path: 'foods',
      select: 'itemTitle itemDescription itemCost category.title',
      populate: {
        path: 'category',
        select: 'title -_id',
      },
    });
  res.set('order', true);
  return res.render('client/dailyOrder', { layout: false, foods: updatedOrder.foods });
});

router.delete('/:date', async (req, res) => {
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const { date } = req.params;
  const order = await Order.findOneAndDelete({ date, userId });
  await Food.updateMany({ _id: { $in: order.foods } }, { $pull: { orders: order._id } });
  const categories = await Category.find({})
    .populate('foods', 'itemTitle itemDescription itemCost');
  res.set('order', false);
  return res.render('client/dailyMenu', { layout: false, categories });
});

module.exports = router;
