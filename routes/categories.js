/* eslint-disable no-underscore-dangle */
const express = require('express');
const jwt = require('jsonwebtoken');
const { Category } = require('../models/Category');
const { Like } = require('../models/Like');
const { Comment } = require('../models/Comment');
const { User } = require('../models/User');
const { Food } = require('../models/Food');
require('../db/mongoose'); // Connect to DB

const router = express.Router();

const getUserLikes = (categories, userId) => categories.map(category => ({
  _id: category._id,
  title: category.title,
  description: category.description,
  liked: category.likes.map(like => like.userId).includes(userId),
  foods: category.foods.map(food => ({
    _id: food._id,
    comments: food.comments,
    itemCost: food.itemCost,
    itemImg: food.itemImg,
    itemTitle: food.itemTitle,
    itemDescription: food.itemDescription,
    liked: food.likes.map(like => like.userId).includes(userId),
  })),
}));

const getUserLike = (food, userId) => food.likes.map(like => like.userId).includes(userId);

const getUserComments = async (user, foodId) => {
  const comments = await Comment.find({ user, foodId });
  return comments.map(comment => comment._id.toString());
};

router.get('/', async (req, res) => {
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const categories = await Category.find()
    .populate('likes', 'userId -_id')
    .populate({
      path: 'foods',
      populate: {
        path: 'likes',
        select: 'userId -_id',
      },
    });
  const sortedCategories = getUserLikes(categories, userId);
  res.render('categories', { layout: false, categories: sortedCategories });
});

// CATEGORY LIKE
router.post('/like', async (req, res) => {
  const { id } = req.body;
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const existedLike = await Like.findOne({ entityId: id, userId });
  if (existedLike) {
    await existedLike.remove();
    const category = await Category.findOneAndUpdate(
      { _id: id },
      { $pull: { likes: existedLike._id } },
      { new: true },
    );
    return res.send(category);
  }
  const like = new Like({
    entity: 'category', userId, entityId: id,
  });
  await like.save();
  const category = await Category.findOneAndUpdate(
    { _id: id },
    { $push: { likes: like._id } },
    { new: true },
  );
  return res.send(category);
});

// FOOD
router.get('/food/:id', async (req, res) => {
  const { id } = req.params;
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const food = await Food.findById(id)
    .populate({
      path: 'comments',
      populate: {
        path: 'user',
        select: 'name',
      },
    })
    .populate('likes', 'userId');
  const userCommentsArray = await getUserComments(userId, food._id);
  const userLike = getUserLike(food, userId);
  return res.render('categories/food', {
    layout: false, food, comments: food.comments, userCommentsArray, userLike,
  });
});

router.post('/food/like', async (req, res) => {
  const { id } = req.body;
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const existedLike = await Like.findOne({ entityId: id, userId });
  if (existedLike) {
    await existedLike.remove();
    const food = await Food.findOneAndUpdate(
      { _id: id },
      { $pull: { likes: existedLike._id } },
      { new: true },
    );
    return res.send(food);
  }
  const like = new Like({
    entity: 'food', userId, entityId: id,
  });
  await like.save();
  const food = await Food.findOneAndUpdate(
    { _id: id },
    { $push: { likes: like._id } },
    { new: true },
  );
  return res.send(food);
});

// FOOD COMMENT
router.post('/food/:id', async (req, res) => {
  const { id } = req.params;
  const { text } = req.body;
  if (!text.trim()) return res.send('Comment should not be empty string');
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;

  const comment = new Comment({
    user: userId, text, foodId: id,
  });
  await comment.save();
  const food = await Food.findOneAndUpdate(
    { _id: id },
    { $push: { comments: comment._id } },
    { new: true },
  );
  await User.findOneAndUpdate(
    { _id: userId },
    { $push: { comments: comment._id } },
  );
  return res.send(food);
});

router.patch('/comment/:id', async (req, res) => {
  const { id } = req.params;
  const { text } = req.body;
  const newText = text.trim();
  if (!newText) return res.send('Comment should not be empty string');
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const comment = await Comment.findById(id);
  if (userId !== comment.user.toString()) return res.send('You have no access to edit this comment');
  comment.set('text', newText);
  await comment.save();
  return res.send(comment);
});

router.delete('/comment/:id', async (req, res) => {
  const { id } = req.params;
  const userData = jwt.decode(req.headers.authorization.split(' ')[1]).data;
  const userId = userData.id;
  const userRole = userData.role;
  const comment = await Comment.findById(id);
  if (userRole !== 'admin' && userId !== comment.user.toString()) return res.send('You have no access to edit this comment');
  await comment.remove();
  const food = await Food.findOneAndUpdate(
    { _id: comment.foodId },
    { $pull: { comments: comment._id } },
    { new: true },
  );
  await User.findOneAndUpdate({ _id: comment.user }, { $pull: { comments: comment._id } });
  return res.send(food);
});

module.exports = router;
