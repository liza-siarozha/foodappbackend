const express = require('express');
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const jwtMiddleware = require('express-jwt-middleware');
const randomstring = require('randomstring');
const { genSalt, hash } = require('bcryptjs');

const { validateRegistration, validateAuthorization } = require('../middlewares/inputValidation');
const { User } = require('../models/User');
const { sendEmail, composeEmail } = require('../helpers/mailer');
const redis = require('../helpers/redis');
const CONFIG = require('../config/index');
require('../db/mongoose'); // Connect to DB

const router = express.Router();

// Generate email verification token
const generateEmailToken = () => ({
  token: randomstring.generate(),
  expiredAt: Date.now() + 1800000, // Expires in half-an-hour
});

// Generate tokens pair
const generateTokensPair = async (userId, userRole = 'client') => {
  const refreshToken = uuid();
  // Generate accessToken
  const accessToken = jwt.sign({
    data: {
      id: userId,
      role: userRole,
    },
  }, CONFIG.jwt_encryption, {
    expiresIn: '1h',
  });
  await redis.storeRefreshToken(userId, refreshToken);
  return { accessToken, refreshToken };
};

router.get('/register', (req, res) => {
  res.render('register', { layout: false });
});

router.post('/register', validateRegistration, async (req, res) => {
  const { email, password, name } = req.body;
  const emailToken = generateEmailToken();
  genSalt(10, (e, salt) => {
    if (e) return res.status(400).send(e);
    return hash(password, salt)
      .then((hashedPass) => {
        // Add user to db
        const user = new User({
          email,
          password: hashedPass,
          name,
          role: 'client',
          emailToken,
          validated: false,
        });
        return user.save();
      })
      .catch(err => res.send(err));
  });

  const emailHTML = composeEmail('http://localhost:3000/auth/verify/', emailToken.token);
  await sendEmail('food-app@mail.com', 'FoodApp verification', email, emailHTML);
  res.send({
    message: 'Please, check your email', email, password, name,
  });
});

router.get('/verify/:token', async (req, res) => {
  const { token } = req.params;
  const user = await User.findOne({ 'emailToken.token': token });

  if (!user) return res.send('Verification error. Link is invalid');

  const expireDate = new Date(user.emailToken.expiredAt);
  const nowDate = new Date();

  if (nowDate > expireDate) {
    return res.send({
      message: 'Expired link',
      // eslint-disable-next-line no-underscore-dangle
      generateLink: `http://localhost:3000/auth/verify/gen/${user._id}`,
    });
  }
  user.set({
    validated: true,
    emailToken: {
      token: null,
      expiredAt: null,
    },
  });
  await user.save();
  return res.send('Succesfully verified');
});

// Generate new confirmation link
router.get('/verify/gen/:userId', async (req, res) => {
  const user = await User.findById(req.params.userId);
  if (user.validated) return res.send('No verification required');

  const emailToken = generateEmailToken();
  user.set({ emailToken });
  await user.save();

  const emailHTML = composeEmail('http://localhost:3000/auth/verify/', emailToken.token);
  await sendEmail('food-app@mail.com', 'FoodApp verification', user.email, emailHTML);
  return res.send('Please, check your email for new verification link');
});

router.get('/login', (req, res) => {
  res.render('login', { layout: false });
});

router.post('/login', validateAuthorization, async (req, res) => {
  const { email } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(404).send('User not found');
  }
  if (!user.validated) {
    return res.send('Please, verify your account');
  }
  // eslint-disable-next-line no-underscore-dangle
  const tokenPair = await generateTokensPair(user._id.toString(), user.role);
  return res.send({ tokenPair, userRole: user.role });
  // Then define both tokens on client
});

// User performs this request on client side in case he gets an error of expired access token.
// Client sends both access and refresh tokens in request's body and header
// If tokens are succesfully redefined then User retry initial query with new pair of tokens
// If tokens weren't redefined then User gets login page
router.post('/refresh', async (req, res) => {
  // Get refreshToken from Local Storage
  const { refreshToken } = req.body;
  // Get userId from Local Storage
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  const user = await User.findById(userId);
  if (!user) {
    return res.status(404).send('User not found');
  }
  const tokenFound = await redis.checkRefreshToken(userId, refreshToken);

  if (tokenFound) {
    await redis.removeRefreshToken(userId, refreshToken);
    const newTokenPair = await generateTokensPair(userId, user.role);
    return res.send(newTokenPair);
    // Then redefine botn tokens on client
  }
  await redis.dropRefreshTokens(userId);
  return res.status(404).send('Authentication error');
  // Then drop both tokens on client
});

router.post('/logout', jwtMiddleware({ secret: CONFIG.jwt_encryption }), async (req, res) => {
  // Get refreshToken from Local Storage
  const { refreshToken } = req.body;
  // Get userId from headers
  const userId = jwt.decode(req.headers.authorization.split(' ')[1]).data.id;
  await redis.removeRefreshToken(userId, refreshToken);
  return res.send('Logged out');
  // Then drop both tokens on client
});

module.exports = router;
