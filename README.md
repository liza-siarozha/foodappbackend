# FoodApp

## Release 1.1.

Admin features:

 - Add, update and remove food categories;
 - Add, update and remove foods;
 - Upload and change food's picture;
 - List of food's comments is available on food edit page. Administrator can remove users' comments.

Customer features:

 - Create orders for the next 30 days;
 - Update and delete orders;
 -  Today's order is available for change until 10 a.m.
 - Customer can like categories and particular foods;
 - Customer can add, update and delete comments for particular foods;
 
 **Bonus:**
 Ranking dashboard page is available. Customer can see three different food selections: 
  - Most liked foods (by all users);
  - Most ever ordered foods (by Customer);
  - Most ordered foods for tommorow (by all users);
---

## For developer

  `.env` file is required. Administrator should provide following credentials:

    APP=foodapp
    PORT=3000
    
    DB_HOST=localhost // Database host
    DB_PORT=27017 // Database port
    DB_NAME=FoodApp // Database name
        
    // It's considered, that admin uses Mailgun service for sending emails. 
    // Api key and domain should be provided.
    MAILGUN_API_KEY=your-mailgun-api-key
    MAILGUN_DOMAIN=your-mailgun-domain
	
    // In case admin uses some different mail service, he/she needs to provide next few credentials.
    // Appropriate changes should be also made in helpers/mailer.js
    MAIL_SERVICE=service-name
    MAIL_USER=user
    MAIL_PASSWORD=password
    
    JWT_ENCRYPTION=your-jwt-secret-key

 Please, run `npm install` and `npm run start` to build and start application.